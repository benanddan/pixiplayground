import {GAME_SIZE} from './consts.js';

class Application extends PIXI.Application {
    /**
     * Construct our game application.
     *
     * @param {Object} options The options to apply to our game application.
     */
    constructor(options = {...GAME_SIZE}) {
        super(options);

        // Add canvas to the document
        document.body.appendChild(this.view);

        // Add our resize event listener.
        window.addEventListener('resize', this.resize);
    }

    /**
     * Resize our game window.
     */
    resize = ()=>{
        console.log("Resize")
    }
}

export default Application;
