/**
 * The initial game loading size.
 * @type {{width: Number, height: Number}}
 */
export const GAME_SIZE = {
    'width': 1920,
    'height': 1080
};
