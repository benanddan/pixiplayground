/**
 * The game's name.
 *
 * @type {String} The game's name.
 */
export const GAME_NAME = 'HELLRAISER';
