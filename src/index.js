import * as BDLib from '../lib';

/**
 * Initialise our game.
 */
function init() {
    // TODO: This should be better, create event system in core/window and fire it to call this function.
    BDLib.core.window.load();

    // We should tidy this up really. OnLoad should call some form of start game which has a game start specific config.
    window.GAME = new BDLib.core.Application();
}

// Wait for our window to load.
window.onload = init;

